const LOREM = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'

function tag(name, ...children) {
    const ele = document.createElement(name)
    ele.attr = (name, value) => {
        ele.setAttribute(name, value)
        return ele
    }
    ele.click = f => {
        ele.onclick = f
        return ele
    }
    ele.addClass = name => {
        ele.classList.add(name)
        return ele
    }
    ele.setStyle = (name, value = '', priority = '') => {
        ele.style.setProperty(name, value, priority)
        return ele
    }
    ele.append(...children)
    return ele
}

const MUNDANE_TAGS = ['article', 'aside', 'blockquote', 'div', 'footer', 'h1',
    'h2', 'h3', 'h4', 'h5', 'h6', 'header', 'main', 'nav', 'p', 'section', 'span',
    'textarea', 'br', 'b', 'details', 'summary', 'ul', 'li', 'script', 'title',
    'hr', 'form', 'button', 'label', 'html', 'head', 'body']
for (let tagName of MUNDANE_TAGS) window[tagName] = (...children) => tag(tagName, ...children)

function a(href, ...children) { return tag('a', ...children).attr('href', href) }
function img(src) { return tag('img').attr('src', src) }
function input(type) { return tag('input').attr('type', type) }
function style(css) { return tag('style', css) }
function link(href, rel = 'stylesheet') { return tag('link').attr('rel', rel).attr('href', href) }

function shadow(name, ...children) {
    const ele = tag(name)
    ele.attachShadow({ mode: 'open' }).append(...children)
    return ele
}

function slugify(str) {
    return str.trim()
        .replace(/[^\w\s-.~]/g, '')
        .replace(/\s/g, '_')
}

function Router() {
    const queue = []
    this.use = function (path, ...callbacks) {
        queue.push({
            pattern: new URLPattern({ pathname: path }),
            middlewares: callbacks,
        })
    }
    this.exec = function () {
        const iterator = (function* () {
            const url = new URL(location.hash.slice(1), location.origin)
            for (let ctx of queue) {
                const match = ctx.pattern.exec(url)
                if (!match) continue
                for (let f of ctx.middlewares) {
                    const flag = yield { callback: f, match: match.pathname.groups }
                    if (flag) break
                }
            }
        })()
        function callNext(flag) {
            const next = iterator.next(flag).value
            if (next) next.callback(next.match, callNext)
        }
        callNext()
    }
    addEventListener('hashchange', this.exec)
}
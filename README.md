# single.js

minimal JavaScript front-end framework, inspired by [Grecha.js](https://github.com/tsoding/grecha.js)

- less than 100 lines of code
- url routing with regular expression, good for building [SPA](https://en.wikipedia.org/wiki/Single-page_application)

**See example.html**

```html
<!DOCTYPE html>
<head><title>single.js</title></head>
<body>
    <div id="entry"></div>
    <script src='single.js'></script>
    <script>
        const index = [
            style(`body { margin: 0; height: 100vh; display: flex; } #entry { margin: auto; }`),
            h1('INDEX'),
            ul(
                li(a('#/lorem', 'lorem')),
                li(a('#/image', 'image')),
            )
        ]

        router.use('/', () => entry.replaceChildren(...index))
        router.use('/lorem', () => entry.replaceChildren(h1('Lorem'), p(LOREM), a('#/', '< Home')))
        router.use('/image/:id(\\d*)*', match => entry.replaceChildren(
            h1('Image ' + match.id),
            getImg(match.id),
            a('#/', '< Home')
        ))
        router.exec()

        function getImg(id) {
            if (!id) {
                return ul(
                    li(a('#/image/80', 'id:80')),
                    li(a('#/image/443', 'id:443')),
                )
            } else {
                return div(
                    a('#/image', '< Go back'),
                    img(`https://picsum.photos/id/${id}/200/300`).setStyle('display', 'block')
                )
            }
        }
    </script>
</body>
```
